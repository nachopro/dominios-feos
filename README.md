# dominios-feos

¿Cansado de pagar una conexión a internet para que el 50% del tráfico usado se haya ido en descargar publicidad no deseada?
¿Harto de que todos sepan qué le gustaría comprar, excepto sus familiares (que siempre regalan pares de media)?
¿Indignado con el derroche energético en servidores de publicidad y seguidores de Cookies?

¡PARE DE SUFRIR!

Dominios Feos está basado en el concepto de "Bloqueo por DNS". Esto significa que su navegador no va a verificar si el dominio en cuestión está en una lista negra, directamente su sistema operativo va a decir "127.0.0.1" transparentemente a su navegador (sea Firefox, Chrome, IE6,7,8,9,10,23423, wget, lynx).
Incluso mejor: Las aplicaciones con publicidad molesta no podrán mostrar esos horroroso píxeles decadentes.

El ejecutable dominios-feos.sh consulta a proveedores gratuios en busca de dominios de Publicidad, Malware, Virus, etc, los descarga y los guarda en hosts.block.

Este archivo, sin más ni menos, puede incluirse en un servidor dnsmasq o puede agregarse al archivo hosts de su sistema (Ya sea Mac, Linux, Windows, iOS o Android).

Fin del asunto ;)
